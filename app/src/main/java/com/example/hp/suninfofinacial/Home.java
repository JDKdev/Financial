package com.example.hp.suninfofinacial;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

public class Home extends AppCompatActivity {

    TextView hpnum, venum, company, name, mobile, dueamt, duespend,status, net_amnt, other_amnt;
    CardView cardView;
    Button f1page, f2page;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        hpnum = (TextView) findViewById(R.id.hpnum);
        venum = (TextView) findViewById(R.id.venum);
        company = (TextView) findViewById(R.id.company);
        name = (TextView) findViewById(R.id.name);
        mobile = (TextView) findViewById(R.id.mobile);
        dueamt = (TextView) findViewById(R.id.dueamt);
        duespend = (TextView) findViewById(R.id.duespend);
        status = (TextView) findViewById(R.id.status);
        other_amnt = (TextView) findViewById(R.id.otheramnt);
        net_amnt = (TextView) findViewById(R.id.netamt);
        f1page = (Button) findViewById(R.id.f1page);
        f2page = (Button) findViewById(R.id.f2page);
        cardView = (CardView) findViewById(R.id.card_view);

        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){
            String da = bundle.getString("data");
            try {
                JSONObject object = new JSONObject(da);
                hpnum.setText("HP No : " + object.getString("hpnum"));
                venum.setText("Vehicle No : " + object.getString("vehnum"));
                company.setText("Company : " + object.getString("compname"));
                name.setText("Name : " + object.getString("ptyname"));
                mobile.setText("Mobile : " + object.getString("phonenum"));
                dueamt.setText("Total Due Amount : " + object.getString("dueamt"));
                duespend.setText("No of Dues Pending :" + object.getString("duespending"));
                other_amnt.setText("Others Amount : "+String.valueOf(object.getString("othersamt")));
                net_amnt.setText("Net Amount : "+String.valueOf(object.getString("netamount")));
                String state = object.getString("status");
                if (state!=null){
                    status.setVisibility(View.VISIBLE);
                    status.setText("Status :" + object.getString("status"));
                }else {
                    status.setVisibility(View.GONE);
                }

                f1page.setVisibility(View.GONE);
                f2page.setVisibility(View.GONE);

                status.setText("Status :" + object.getString("status"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        int[] androidColors = getResources().getIntArray(R.array.androidcolors);
        int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
        cardView.setBackgroundColor(randomAndroidColor);
    }
}
