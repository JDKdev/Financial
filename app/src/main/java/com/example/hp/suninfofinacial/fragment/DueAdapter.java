package com.example.hp.suninfofinacial.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hp.suninfofinacial.ImageZoom;
import com.example.hp.suninfofinacial.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by Hp on 09-04-2018.
 */

public class DueAdapter extends RecyclerView.Adapter<DueAdapter.Myholder> {
    Context context;
    List<DueModel> partyListModels;
    AddTouchListen addTouchListen;


    public interface AddTouchListen {
        public void onClick(int position, List<String> mobileList);
    }
    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }


    public DueAdapter(Context partyList, List<DueModel> partyListModelList) {
        this.context = partyList;
        this.partyListModels = partyListModelList;
    }

    @Override
    public DueAdapter.Myholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_home, parent, false);

        return new Myholder(itemView);
    }

    @Override
    public void onBindViewHolder(final DueAdapter.Myholder holder, final int position) {
        holder.com_nam.setText("Company name : "+partyListModels.get(position).getCompanyname());
        holder.pty_name.setText("Party name : "+partyListModels.get(position).getPtyname());
        holder.veh_num.setText("Vehicle no : "+partyListModels.get(position).getVehnum());
        holder.hp_num.setText("Hp no : "+String.valueOf(partyListModels.get(position).getHpnum()));
        holder.mob_no.setText("mob.no : "+String.valueOf(partyListModels.get(position).getPhonenum()));
        holder.due_pending.setText("Dues Pending : "+String.valueOf(partyListModels.get(position).getDuespending()));
        holder.due_amnt.setText("Due Amount : "+String.valueOf(partyListModels.get(position).getDueamt()));
        holder.other_amnt.setText("Others Amount : "+String.valueOf(partyListModels.get(position).getOthersamt()));
        holder.net_amnt.setText("Net Amount : "+String.valueOf(partyListModels.get(position).getNetamount()));

        if (!partyListModels.get(position).getF1Page().isEmpty()){
            holder.f1page.setVisibility(View.VISIBLE);
        }else {
            holder.f1page.setVisibility(View.GONE);
        }
        if (!partyListModels.get(position).getF2Page().isEmpty()){
            holder.f2page.setVisibility(View.VISIBLE);
        }else {
            holder.f2page.setVisibility(View.GONE);
        }

        holder.f1page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ImageZoom.class);
                intent.putExtra("image", partyListModels.get(position).getF1Page());
                context.startActivity(intent);
            }
        });
        holder.f2page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ImageZoom.class);
                intent.putExtra("image", partyListModels.get(position).getF2Page());
                context.startActivity(intent);
            }
        });

        String statusstring = partyListModels.get(position).getStatus();
        if (statusstring!= null){
            holder.status.setVisibility(View.VISIBLE);
            holder.net_amnt.setText("Status : "+String.valueOf(partyListModels.get(position).getStatus()));
        }else {
            holder.status.setVisibility(View.GONE);
        }

        holder.fabcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> mobileList = new ArrayList<>();
                String mobile = partyListModels.get(position).getPhonenum();
                String[] allIdsArray = TextUtils.split(mobile, ",");
                        mobileList = new ArrayList<String>(Arrays.asList(allIdsArray));
                        for(String element : mobileList){
                            Log.i("Result element :", element);
                            if (!element.isEmpty()){
                                if(!mobileList.contains(element)) {
                                    mobileList.add(element);
                                }
                            }
                        }


                if (mobileList.size() > 1){
                    holder.fabcall.setVisibility(View.VISIBLE);
                    if (addTouchListen!=null){
                        addTouchListen.onClick(position, mobileList);
                    }
                } else if(mobileList.size()==0){
                    Toast.makeText(context, "Mobile Number doesnot exist", Toast.LENGTH_SHORT).show();
                } else {
                    holder.fabcall.setVisibility(View.VISIBLE);
                    String mob = mobileList.get(0).toString();
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    callIntent.setData(Uri.parse("tel:" +mob));
                    context.startActivity(callIntent);
                }
            }
        });

        int[] androidColors = context.getResources().getIntArray(R.array.androidcolors);
        int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
        holder.cardView.setBackgroundColor(randomAndroidColor);
        final String body = "Company name : " + partyListModels.get(position).getCompanyname() + "\n" +
                "Vehicle no : "+ partyListModels.get(position).getVehnum() + "\n" +
                "Hp no : "+String.valueOf(partyListModels.get(position).getHpnum()) + "\n" +
                "mob.no : "+String.valueOf(partyListModels.get(position).getPhonenum()) + "\n" +
                "Due Amount : "+String.valueOf(partyListModels.get(position).getDueamt()) + "\n" +
                "Dues Pending : "+String.valueOf(partyListModels.get(position).getDuespending())+ "\n" +
                "Other Amount : "+String.valueOf(partyListModels.get(position).getOthersamt()) + "\n" +
                "Net Amount : "+String.valueOf(partyListModels.get(position).getNetamount());
        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                holder.contentlayout.setAlpha((float) 0.5);
                holder.shaarelayout.setVisibility(View.VISIBLE);
                holder.share.setVisibility(View.VISIBLE);
                holder.share.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        String shareBodyText = body;
                        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject/Title");
                        intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBodyText);
                        context.startActivity(Intent.createChooser(intent, "Choose sharing method"));
                    }
                });
                holder.shaarelayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.shaarelayout.setVisibility(View.GONE);
                        holder.contentlayout.setAlpha(1);
                    }
                });

                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return partyListModels.size();
    }

    public class Myholder extends RecyclerView.ViewHolder {
        CardView cardView;
        ImageView fabcall;
        ImageButton share;
        RelativeLayout contentlayout,shaarelayout;
        TextView com_nam,pty_name,veh_num,hp_num,mob_no,due_pending,due_amnt,other_amnt,net_amnt,status;
        Button f1page, f2page;
        public Myholder(View itemView) {
            super(itemView);

            cardView = (CardView)itemView.findViewById(R.id.card_view);
            fabcall = (ImageView) itemView.findViewById(R.id.fab_call);
            contentlayout = (RelativeLayout) itemView.findViewById(R.id.contentlayout);
            shaarelayout = (RelativeLayout) itemView.findViewById(R.id.sharelayout);
            share = (ImageButton) itemView.findViewById(R.id.sharebutton);
            f1page = (Button) itemView.findViewById(R.id.f1page);
            f2page = (Button) itemView.findViewById(R.id.f2page);

            com_nam=(TextView)itemView.findViewById(R.id.company);
            pty_name=(TextView)itemView.findViewById(R.id.name);
            veh_num=(TextView)itemView.findViewById(R.id.venum);
            hp_num=(TextView)itemView.findViewById(R.id.hpnum);
            mob_no=(TextView)itemView.findViewById(R.id.mobile);
            due_pending=(TextView)itemView.findViewById(R.id.duespend);
            due_amnt=(TextView)itemView.findViewById(R.id.dueamt);
            other_amnt=(TextView)itemView.findViewById(R.id.otheramnt);
            net_amnt=(TextView)itemView.findViewById(R.id.netamt);
            status=(TextView)itemView.findViewById(R.id.status);
        }
    }
}

