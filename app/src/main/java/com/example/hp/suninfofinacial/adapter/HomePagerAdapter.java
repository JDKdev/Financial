package com.example.hp.suninfofinacial.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.hp.suninfofinacial.fragment.AddDueFragment;
import com.example.hp.suninfofinacial.fragment.DueFragment;


public class HomePagerAdapter extends FragmentPagerAdapter {

    private DueFragment dueFragment;
    private AddDueFragment addDueFragment;
   // private MapsActivity mapsActivity;
    Context context;
    FragmentManager fragmentManager;
    /*private RatingFragment ratingFragment;
    private SettingFragment settingFragment;*/

    public HomePagerAdapter(FragmentManager fm) {
        super(fm);
    }



    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                if (dueFragment == null) {
                    dueFragment = new DueFragment();
                }
                return dueFragment;
            case 1:
                if (addDueFragment == null) {
                    addDueFragment = new AddDueFragment();
                }
                return addDueFragment;
                default:
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
