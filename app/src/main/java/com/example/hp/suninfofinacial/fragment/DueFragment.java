package com.example.hp.suninfofinacial.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.hp.suninfofinacial.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by gotoh on 09-04-2018.
 */

public class DueFragment extends Fragment {

    RecyclerView dur_recycler;
    EditText fromdue,todue;
    Button submit;
    DueAdapter dueAdapter;
    List<DueModel>dueModels;
    ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
              View rootView = inflater.inflate(R.layout.due_fragment, null);

        dur_recycler =  (RecyclerView)rootView.findViewById(R.id.due_recycle);
        todue =  (EditText) rootView.findViewById(R.id.todue);
        LinearLayout mainLayout;

// Get your layout set up, this is just an example
        mainLayout = (LinearLayout)rootView.findViewById(R.id.duerange);

// Then just use the following:

        fromdue = (EditText)rootView.findViewById(R.id.fromdue);
        submit = (Button)rootView.findViewById(R.id.submitdue);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissActivity();
                Listdues();
            }
        });
        return rootView;
    }

    private void dismissActivity() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != getActivity().getCurrentFocus())
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus()
                    .getApplicationWindowToken(), 0);

    }

    private void Listdues() {
        String url = "http://103.79.74.15/demowepapi/api/demo/getalldue?fromdue=" + fromdue.getText().toString() + "&todue=" + todue.getText().toString();
        final ProgressDialog dialog1 = ProgressDialog.show(getActivity(), "Sun Info Technologies", "Please Wait...", true);
        AndroidNetworking.get(url)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        List<DueModel>dueModels= new ArrayList<>();
                        try {
                        for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                DueModel dueModel = new DueModel();
                                dueModel.setCompanyname(jsonObject.getString("compname"));
                                dueModel.setPtyname(jsonObject.getString("ptyname"));
                                dueModel.setVehnum(jsonObject.getString("vehnum"));
                                dueModel.setHpnum(jsonObject.getString("hpnum"));
                                dueModel.setPhonenum(jsonObject.getString("phonenum"));
                                dueModel.setDuespending(jsonObject.getString("duespending"));
                                dueModel.setDueamt(jsonObject.getString("dueamt"));
                                dueModel.setOthersamt(jsonObject.getString("othersamt"));
                                dueModel.setNetamount(jsonObject.getString("netamount"));
                                dueModel.setStatus(jsonObject.getString("status"));
                                dueModel.setF1Page(jsonObject.getString("F1page"));
                                dueModel.setF2Page(jsonObject.getString("F2page"));
                                //String stat = jsonObject.getString("status");

                                dueModels.add(dueModel);


                            }
                            dueAdapter = new DueAdapter(getActivity(), dueModels);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                            dur_recycler.setLayoutManager(mLayoutManager);
                            dur_recycler.setItemAnimator(new DefaultItemAnimator());
                            dur_recycler.setAdapter(dueAdapter);
                            dueAdapter.setOnClickListen(new DueAdapter.AddTouchListen() {
                                @Override
                                public void onClick(int position, final List<String> mobileList) {
                                    final List<String> molist = new ArrayList<>();
                                    for (int i = 0; i < mobileList.size(); i++) {
                                        molist.add(mobileList.get(i).toString());
                                    }
                                    System.out.println(mobileList.size() + molist.size());

                                    listView = new ListView(getActivity());
                                    ArrayAdapter<String> adapter=new ArrayAdapter<String>(getActivity(), R.layout.list_item, R.id.txtitem, molist);
                                    listView.setAdapter(adapter);
                                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                            String mob = molist.get(position).toString();
                                            Intent callIntent = new Intent(Intent.ACTION_DIAL);
                                            callIntent.setData(Uri.parse("tel:" + mob));
                                            startActivity(callIntent);
                                        }
                                    });
                                    ShowDialog();
                                }
                            });
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog1.dismiss();

                    }
                    @Override
                    public void onError(ANError anError) {
                        dialog1.dismiss();

                    }
                });
    }

    private void ShowDialog() {
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);
        builder.setTitle("Mobile Number List");
        //builder.setPositiveButton("OK",null);
        builder.setView(listView);
        AlertDialog dialog=builder.create();
        dialog.show();
    }


    public void onRefresh() {

    }
}
