package com.example.hp.suninfofinacial.fragment;

import java.io.Serializable;

/**
 * Created by Hp on 09-04-2018.
 */

public class DueModel  {

    String companyname;
    String ptyname;
    String vehnum;
    String hpnum;
    String phonenum;
    String duespending;
    String dueamt;
    String othersamt;
    String netamount;
    String status;
    String F1Page;
    String F2Page;

    public String getF1Page() {
        return F1Page;
    }

    public void setF1Page(String f1Page) {
        F1Page = f1Page;
    }

    public String getF2Page() {
        return F2Page;
    }

    public void setF2Page(String f2Page) {
        F2Page = f2Page;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getPtyname() {
        return ptyname;
    }

    public void setPtyname(String ptyname) {
        this.ptyname = ptyname;
    }

    public String getVehnum() {
        return vehnum;
    }

    public void setVehnum(String vehnum) {
        this.vehnum = vehnum;
    }

    public String getHpnum() {
        return hpnum;
    }

    public void setHpnum(String hpnum) {
        this.hpnum = hpnum;
    }

    public String getPhonenum() {
        return phonenum;
    }

    public void setPhonenum(String phonenum) {
        this.phonenum = phonenum;
    }

    public String getDuespending() {
        return duespending;
    }

    public void setDuespending(String duespending) {
        this.duespending = duespending;
    }

    public String getDueamt() {
        return dueamt;
    }

    public void setDueamt(String dueamt) {
        this.dueamt = dueamt;
    }

    public String getOthersamt() {
        return othersamt;
    }

    public void setOthersamt(String othersamt) {
        this.othersamt = othersamt;
    }

    public String getNetamount() {
        return netamount;
    }

    public void setNetamount(String netamount) {
        this.netamount = netamount;
    }


    }
