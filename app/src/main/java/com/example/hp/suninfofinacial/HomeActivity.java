package com.example.hp.suninfofinacial;

import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.hp.suninfofinacial.adapter.HomePagerAdapter;
import com.example.hp.suninfofinacial.fragment.DueFragment;


public class HomeActivity extends AppCompatActivity {

    private ViewPager pager;
    private TabLayout tabLayout;
    private HomePagerAdapter adapterPager;
    private TextView customTab;
    private LayoutInflater inflater;
    protected int selectableItemBackground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homescreen);

        TypedValue typedValue = new TypedValue();
        getTheme().resolveAttribute(R.attr.selectableItemBackground, typedValue, true);
        selectableItemBackground = typedValue.resourceId;

        initViews();
    }

    private void initViews() {
        pager = (ViewPager) findViewById(R.id.pager_home);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout_home);

        if (inflater == null)
            inflater = getLayoutInflater();

        customTab = (TextView) inflater.inflate(R.layout.custom_tab, null);
        customTab.setText("Due");
        customTab.setTextColor(Color.parseColor("#FFFFFF"));
        customTab.setBackgroundResource(R.drawable.btn_click_app_rectangle_with_semicircle_edge);
        tabLayout.addTab(tabLayout.newTab()
                .setText("Due")
                //                .setIcon(R.drawable.ic_action_popular)
                .setCustomView(customTab));

        customTab = (TextView) inflater.inflate(R.layout.custom_tab, null);
        customTab.setText("Get Location");
        customTab.setTextColor(Color.parseColor("#FFFFFF"));
        tabLayout.addTab(tabLayout.newTab()
                .setText("Get Location")
                //                .setIcon(R.drawable.ic_action_popular)
                .setCustomView(customTab));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
                try {
                    tab.getCustomView().setBackgroundResource(R.drawable.btn_click_app_rectangle_with_semicircle_edge);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                try {
                    tab.getCustomView().setBackgroundResource(selectableItemBackground);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }

        });
        adapterPager = new HomePagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapterPager);

        pager.setCurrentItem(0);
        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
        pager.setOffscreenPageLimit(2);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((DueFragment) adapterPager.getItem(0)).onRefresh();
    }
}
