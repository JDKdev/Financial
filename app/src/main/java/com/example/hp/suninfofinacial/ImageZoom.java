package com.example.hp.suninfofinacial;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jsibbold.zoomage.ZoomageView;
import com.squareup.picasso.Picasso;

public class ImageZoom extends AppCompatActivity {

    ZoomageView zoompage;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_zoom);
        zoompage = (ZoomageView) findViewById(R.id.zoompage);

        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){
            url = bundle.getString("image");
            Picasso.with(this).load(url)
                    .placeholder(R.drawable.loader)
                    .into(zoompage);
        }
    }
}
