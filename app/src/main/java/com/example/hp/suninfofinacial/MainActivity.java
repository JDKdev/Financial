package com.example.hp.suninfofinacial;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    EditText vno, hpno;
    Button sub;
    TextView staff_login;
    String answer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        vno = (EditText) findViewById(R.id.vno);
        hpno = (EditText) findViewById(R.id.hpno);
        sub = (Button) findViewById(R.id.submit);
        staff_login = (TextView) findViewById(R.id.staff_login);
        ConnectivityManager cm = (ConnectivityManager) getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                answer="You are connected to a WiFi Network";
            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                answer="You are connected to a Mobile Network";
            init();
        }
        else
            answer = "No internet Connectivity";
        Toast.makeText(getApplicationContext(), answer, Toast.LENGTH_LONG).show();

        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!vno.getText().toString().isEmpty()){
                    vno();
                } else if (!hpno.getText().toString().isEmpty()){
                    init();
                } else if (!vno.getText().toString().isEmpty() || !hpno.getText().toString().isEmpty()){
                    vno();
                } else {
                    Toast.makeText(MainActivity.this, "Please enter atleast one field!", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private void init() {
        if (!hpno.getText().toString().isEmpty()){
            String url = "http://103.79.74.15/demowepapi/api/demo/gemaster?hpnum=" + hpno.getText().toString();
            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {
                            try {
                                if (response.length() > 0) {
                                    JSONObject object = response.getJSONObject(0);
                                    Intent intent = new Intent(MainActivity.this, Home.class);
                                    intent.putExtra("data", String.valueOf(object));
                                    startActivity(intent);
                                }else {
                                    Toast.makeText(MainActivity.this, "No data found!", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("in","Error:"+anError.getErrorCode());
                        }
                    });
        }else {
            Toast.makeText(this, "Enter HP number", Toast.LENGTH_SHORT).show();
        }
        staff_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,HomeActivity.class));
            }
        });
    }

    private void vno() {
        if (!vno.getText().toString().isEmpty()){
            String url = "http://103.79.74.15/demowepapi/api/demo/gemaster?vehnum=" + vno.getText().toString();
            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {
                            try {
                                if (response.length() > 0) {
                                    JSONObject object = response.getJSONObject(0);
                                    Intent intent = new Intent(MainActivity.this, Home.class);
                                    intent.putExtra("data", String.valueOf(object));
                                    startActivity(intent);
                                }else {
                                    Toast.makeText(MainActivity.this, "No data found!", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("in","Error:"+anError.getErrorCode());
                        }
                    });
        }else {
            Toast.makeText(this, "Enter HP number", Toast.LENGTH_SHORT).show();
        }
        staff_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,HomeActivity.class));
            }
        });
    }
}


